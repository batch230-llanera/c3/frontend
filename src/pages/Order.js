import { useState, useEffect, useContext } from "react";

import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate, Link, Navigate } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function Order (){

  const { user } = useContext(UserContext);

  const  {_id}  = useParams();

  const navigate = useNavigate();

  const [orderStatus, setOrderStatus] = useState('');
  const [totalAmount, setTotalAmount] = useState(0);
  const [purchasedOn, setPurchasedOn] = useState('');
  const [userInfo, setUserInfo] = useState([]);
  const [products, setProducts] = useState([]);

  useEffect(()=>{
    console.log(_id);

    fetch(`${ process.env.REACT_APP_API_URL }/orders/${_id}`)
    .then(res => res.json())
    .then(data => {

      console.log(data);

      setOrderStatus(data.orderStatus);
      setTotalAmount(data.totalAmount);
      setPurchasedOn(data.purchasedOn);
      setUserInfo(data.userInfo.map((user, index) => {
        return (
          <Container fluid className="p-3">
          <Card key={index}>
            <Card.Body>
              <Card.Title>Customer Information:</Card.Title>
              <Card.Text>
              Name: {user.fullName} <br/>
              Address: {user.address} <br/>
              Mobile Number: {user.mobileNumber}
              </Card.Text>
           
            </Card.Body>
          </Card>
          </Container>
          )
      }))
      
      setProducts(data.products.map((product, index) => {
              return (
              
                  <Card.Body key={index}>
                    <Card.Text>
                    <b>{product.name}</b><br/>
                    Quantity: {product.quantity} <br/>
                    Price: {product.price}  <br/>
                    SubTotal: {product.subTotal}
                    </Card.Text>
                  </Card.Body>

        
          )
      }))

  
    })
  }, []);



  return(
    (user.isAdmin)
    ?
      <Navigate to="/admin" />
    : 

    <Container fluid>
    <h1 className="text-center m-3">Order Receipt</h1>
        <Row className="justify-content-center" xs={1} md={2} lg={4}>
        <Col>
          <>
          {userInfo.map((user) => {
            return (
              <div >
                {user}
              </div>
            )
          })}
          </>
          <Container fluid className="p-3">
          <Card>
            <Card.Body>
              <Card.Title>Order Summary:</Card.Title>
              <Card.Text>
              <b>Total Amount: {totalAmount} </b>
              </Card.Text>
           
            </Card.Body>
          </Card>
          </Container>
          
        </Col>
        <Col>
          <Container fluid className="p-3">
            <Card className="p-3">
            <Card.Title>Order Details:</Card.Title>
              {products.map((product) => {
                return (
                  <div >
                    {product}
                 </div>
                )
              })}
            </Card>
          </Container>
        </Col>        
        </Row>
      </Container>
  )
}








