import { useState, useEffect, useContext } from "react";

import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function ProductView(){

	const { user } = useContext(UserContext);

	const [userId, setUserId] = useState(localStorage.getItem("userId"));

	const  {_id}  = useParams();

	const navigate = useNavigate();


	const [name, setName] = useState('');
	const [productImage, setProductImage] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [orderSlotsAvailable, setOrderSlotsAvailable] = useState(0);

	useEffect(()=>{
		console.log(_id);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${_id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setProductImage(data.productImage);
			setDescription(data.description);
			setPrice(data.price);
			setOrderSlotsAvailable(data.orderSlotsAvailable)

		});

	}, [_id])

	const placeOrder = (_id) =>{

		fetch(`${ process.env.REACT_APP_API_URL }/orders/orderPlaced`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				_id: _id,
				orderStatus: 'order placed'
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log("Order data: ")
			console.log(data);

			if(data){
				

				Swal.fire({
					title: "Successfully ordered",
					icon: "success",
					text: "You successfully placed an order"
				});
			
				navigate(`/orders/${data.Cart._id}`);
			}

			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}

	return(
		<Container fluid className="mt-5">
					<Row className="justify-content-center">
						<Col  xs={10} md={8} lg={4}>
							<Card>
								<Card.Body style={{backgroundColor: "#EAE7FA"}} className="text-center">
									<Card.Title style={{fontFamily: "Papyrus, fantasy", fontSize: 28, color: "#D9A1A0"}}>{name}</Card.Title>
									<Card.Img variant="top" src={productImage} className="mb-3" />
									<Card.Subtitle style={{fontFamily: "Sacramento, cursive", fontSize: 20, color: "#D9A1A0"}}>{description}</Card.Subtitle><br/>
									<Card.Text style={{fontFamily: "Helvetica, sans-serif", fontSize: 18,}}>Price: PhP {price}<br/>
									Order Slots: {orderSlotsAvailable}
									</Card.Text>
									{
										(user.id !== null || userId !== null)
										?
											<Button style={{backgroundColor: "#A16AE8", borderColor: "#D9A1A0"}} variant="primary"  size="lg" onClick={() => placeOrder(_id)}>Order</Button>
										:
											<Button style={{backgroundColor: "#A16AE8", borderColor: "#D9A1A0"}} as={Link} to="/login" variant="success"  size="lg">Login to Order</Button>
									}
								</Card.Body>		
							</Card>
						</Col>
					</Row>
				</Container>

	)
}




