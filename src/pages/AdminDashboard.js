import {useContext, useEffect, useState} from "react";
import FileBase64 from 'react-file-base64';

import { Button, Table, Modal, Form, Container, Row, Col} from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function AdminDashboard(){

	//const { user } = useContext(UserContext);

	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));

	
	const [allProducts, setAllProducts] = useState([]);

	const [_id, set_id] = useState("");
	const [name, setName] = useState("");
	const [productImage, setProductImage] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [orderSlotsAvailable, setOrderSlotsAvailable] = useState(0);

    const [isActive, setIsActive] = useState(false);

   
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

	
	const openEdit = (_id) => {
		set_id(_id);

		
		fetch(`${ process.env.REACT_APP_API_URL }/products/${_id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			
			setName(data.name);
			setDescription(data.description);
			setProductImage(data.productImage);
			setPrice(data.price);
			setOrderSlotsAvailable(data.orderSlotsAvailable);
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		
	    setName('');
	    setDescription('');
	    setProductImage('');
	    setPrice(0);
	    setOrderSlotsAvailable(0);

		setShowEdit(false);
	};


	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.orderSlotsAvailable}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								
								(product.isActive)
								?
									<Button  style={{backgroundColor: "#D9A1A0", borderColor: "#D9A1A0", fontSize: 10}} size="sm" onClick={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button style={{backgroundColor: "#A16AE8", borderColor: "#D9A1A0", fontSize: 10}} size="sm" className="mx-1" onClick={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button style={{backgroundColor: "#A58CB3", borderColor: "#D9A1A0", fontSize: 10}} size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}


	useEffect(()=>{
		fetchData();
	}, [])


	const archive = (_id, productName) =>{
		console.log(_id);
		console.log(productName);

		
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archiveProduct`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});
		
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	const unarchive = (_id, productName) =>{
		console.log(_id);
		console.log(productName);

		
		fetch(`${process.env.REACT_APP_API_URL}/products//${_id}/archiveProduct`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
		
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	const addProduct = (e) =>{
			
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    productImage: productImage,
				    description: description,
				    price: price,
				    orderSlotsAvailable: orderSlotsAvailable
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});

		    	
		    		fetchData();
		    		
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    
		    setName('');
		    setProductImage('');
		    setDescription('');
		    setPrice(0);
		    setOrderSlotsAvailable(0);
	}


	const updateProduct = (e) =>{
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/updateProduct`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    productImage: productImage,
				    description: description,
				    price: price,
				    orderSlotsAvailable: orderSlotsAvailable
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully updated",
		    		    icon: "success",
		    		    text: `${name} is now updated`
		    		});

		    		fetchData();
	
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })

		
		    setName('');
		    setProductImage('');
		    setDescription('');
		    setPrice(0);
		    setOrderSlotsAvailable(0);
	} 

	
	useEffect(() => {

       
        if(name !== "" && productImage !== "" && description !== "" && price > 0 && orderSlotsAvailable > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, productImage, description, price, orderSlotsAvailable]);

	return(
		(userRole)
		?
		<>
			
			<div className="mt-5 mb-3 text-center">
				<h1 style={{fontFamily: "Lucida Handwriting, cursive", color: "#D9A1A0"}}>Admin Dashboard</h1>
				
				<Button style={{backgroundColor: "#A16AE8", borderColor: "#D9A1A0"}} className="
				mx-2" onClick={openAdd}>Add Product</Button>
				
				<Button style={{backgroundColor: "#A58CB3", borderColor: "#D9A1A0"}} className="
				mx-2">Show Products</Button>
			</div>
		
			<Table striped bordered hover>
		      <thead style={{backgroundColor: "#EAE7FA", borderColor: "#D9A1A0", fontSize: 10}}>
		        <tr >
		          <th>Product ID</th>
		          <th>Product Name</th>
		          <th>Description</th>
		          <th>Price</th>
		          <th>Order Slots Available</th>
		          <th>Status</th>
		          <th>Actions</th>
		        </tr>
		      </thead>
		      <tbody style={{backgroundColor: "#EAE7FA", borderColor: "#D9A1A0", fontSize: 10}}>
	        	{allProducts}
		      </tbody>
		    </Table>
	
	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
	        	<Container fluid className="m-5">
	        	<Row>
	        	<Col>
	    		<Form onSubmit={e => addProduct(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Product Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Name" 
	    		                value = {name}
	    		                onChange={e => setName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Product Image</Form.Label>

	    	            <div className="container">
	    	                <pre>{JSON.stringify(productImage, null, '\t')}</pre>
	    	                
	    	                  <FileBase64
	    	                    type="file"
	    	                    multiple={false}
	    	                    onDone={({ base64 }) => setProductImage(base64)}
	    	                  />
	    	                  <div className="right-align">
	    	                  </div>

	    	               
	    	              </div>
	    	            </Form.Group>

	    	             <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Product Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter Product Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Product Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="orderSlotsAvailable" className="mb-3">
	    	                <Form.Label>Order Slots Available</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Order Slots" 
	    		                value = {orderSlotsAvailable}
	    		                onChange={e => setOrderSlotsAvailable(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>
	    		</Col>
	    		</Row>
	    		</Container>
	    	</Modal>
	  
        <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    		<Form onSubmit={e => updateProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Product Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Product Name" 
    		                value = {name}
    		                onChange={e => setName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Product Image</Form.Label>
    	            	
    	                <div className="container">
    	                    <pre>{JSON.stringify(productImage, null, '\t')}</pre>
    	                    
    	                      <FileBase64
    	                        type="file"
    	                        multiple={false}
    	                        onDone={({ base64 }) => setProductImage(base64)}
    	                      />
    	                      <div className="right-align">
    	                      </div>

    	                   
    	                  </div>

    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Product Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter Product Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Product Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="orderSlotsAvailable" className="mb-3">
    	                <Form.Label>Order Slots Available</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Order Slots" 
    		                value = {orderSlotsAvailable}
    		                onChange={e => setOrderSlotsAvailable(e.target.value)}
    		                required
    	                />
    	            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>
 
		</>
		:
		<Navigate to="/products" />
	)
}


