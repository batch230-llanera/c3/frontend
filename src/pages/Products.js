import { useEffect, useState, useContext } from "react";

import { Button, Card, Row, Container, Col} from 'react-bootstrap';

import { Navigate } from "react-router-dom";

import ProductCard from "../components/ProductCard";
import UserContext from "../UserContext";

export default function Products() {

	const { user } = useContext(UserContext);


	const [product, setProduct] = useState([]);

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProduct(data.map(product =>{
				return(
				
  						<ProductCard key={product._id} productProp={product} />
  				
  	
				);
			}));
		})
	}, []);



	return(
		(user.isAdmin)
		?
			<Navigate to="/admin" />
		:	
		<Container fluid>
			<h1 className="text-center m-3" style={{fontFamily: "Lucida Handwriting, cursive", color: "#A58CB3"}}>Our Cakes</h1>
				<Row className="justify-content-center" xs={2} md={6} lg={8}>
					{product}
				</Row>
  		</Container>
	)
}

