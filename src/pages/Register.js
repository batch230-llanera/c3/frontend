
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';

import Swal from 'sweetalert2'

export default function Register(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();


	const [fullName, setFullName] = useState('');
	const [address, setAddress] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);

	console.log(fullName);
	console.log(address);
	console.log(mobileNumber);
	console.log(email);
	console.log(password);
	console.log(password2);

	useEffect(() =>{

	   if((fullName !== '' && address !=='' && mobileNumber !== '' && email !== '' && password !== '' && password2 !=='') && (password === password2)){
	            setIsActive(true);
	        }
	        else{
	            setIsActive(false);
	        }

	    }, [fullName, address, email, mobileNumber, password, password2])


	function registerUser(e){
	       e.preventDefault();

	       fetch(`${process.env.REACT_APP_API_URL}/users/emailAvailability`, {
	           method: "POST",
	           headers:{
	               "Content-Type": "application/json"
	           },
	           body: JSON.stringify({
	               email: email
	           })
	       })
	       .then(res => res.json())
	       .then(data =>{
	           console.log(data);

	           if(data){
	               Swal.fire({
	                   title: "Duplicate email found",
	                   icon: "error",
	                   text: "Kindly provide another email to complete the registration."
	               })
	           }
	           else{

	               fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
	                   method: "POST",
	                   headers:{
	                       "Content-Type": "application/json"
	                   },
	                   body: JSON.stringify({
							fullName: fullName,
							address: address,
							mobileNumber: mobileNumber,
							email: email,
							password: password,
							password2: password2
	                   })
	               })
	               .then(res => res.json())
	               .then(data => {
	                   console.log(data);

	                   if(data){
	                       Swal.fire({
	                           title: "Registration Successful",
	                           icon: "success",
	                           text: "Welcome to Leddie's Homemade Sweets!"
	                       });

	                       setFullName('');
	                       setAddress('');
	                       setMobileNumber('');
	                       setEmail('');
	                       setPassword('');
	                       setPassword2('');

	                       navigate("/login");
	                   }
	                   else{

	                       Swal.fire({
	                           title: "Something went wrong",
	                           icon: "error",
	                           text: "Please try again."
	                       });

	                   }
	               })


	           }
	       })

	   }



	return(
		(user.id !== null)
		?
		<Navigate to="/products" />
		: 
		<Row className="justify-content-center" xs={1} md={2} lg={10}>
		<Col>
		<Container fluid className="border m-3" style={{backgroundColor: "#EAE7FA"}} >
		<Form onSubmit={(event) => registerUser(event)} >
		<h3 className="mt-3 mb-3" style={{fontFamily: "Papyrus, fantasy", fontSize: 24, color: "#A58CB3"}}>Register</h3>
			
			<Row className="mb-3">
			<Form.Group as={Col}controlId="userFullName">
			    <Form.Label >Full Name</Form.Label>
			    <Form.Control 
			    	style={{fontFamily: "Helvetica, sans-serif", fontSize: 14}}
			        type="fullName" 
			        placeholder="Enter Full Name" 
			        value = {fullName}
			        onChange = {event => setFullName(event.target.value)}
			        required
			    />
			</Form.Group>

			 <Form.Group as={Col} controlId="mobileNumber">
	            <Form.Label>Mobile Number</Form.Label>
	            <Form.Control
	            	style={{fontFamily: "Helvetica, sans-serif", fontSize: 14}}
	                type="mobileNumber" 
	                placeholder="Enter mobile no" 
	                value={mobileNumber} 
	                onChange={event => setMobileNumber(event.target.value)}
	                required
	            />
	        </Form.Group>
	        </Row>

			<Form.Group className="mb-3" controlId="userAddress">
			    <Form.Label>Address</Form.Label>
			    <Form.Control 
			    	style={{fontFamily: "Helvetica, sans-serif", fontSize: 14}}
			        type="address" 
			        placeholder="Enter Address" 
			        value = {address}
			        onChange = {event => setAddress(event.target.value)}
			        required
			    />
			</Form.Group>

			
	        <Form.Group controlId="userEmail">
	            <Form.Label>Email address</Form.Label>
	            <Form.Control
	            	style={{fontFamily: "Helvetica, sans-serif", fontSize: 14}}
	                type="email" 
	                placeholder="Enter email" 
	                value = {email}
	                onChange = {event => setEmail(event.target.value)}
	                required
	            />
	            <Form.Text className="text-muted" style={{fontFamily: "Lucinda Handwriting, cursive", fontSize: 12}}>
	                We'll never share your email with anyone else.
	            </Form.Text>
	        </Form.Group><br/>

	        <Row className="mb-3">
	        <Form.Group as={Col} controlId="password">
	            <Form.Label>Password</Form.Label>
	            <Form.Control 
	            	style={{fontFamily: "Helvetica, sans-serif", fontSize: 14}}
	                type="password" 
	                placeholder="Password" 
	                value={password} 
	          		onChange={event => setPassword(event.target.value)}
	                required
	            />
	        </Form.Group><br />

	          <Form.Group as={Col} controlId="password2">
	            <Form.Label>Verify Password</Form.Label>
	            <Form.Control 
	            	style={{fontFamily: "Helvetica, sans-serif", fontSize: 14}}
	                type="password2" 
	                placeholder="Verify Password" 
	                value={password2} 
	          		onChange={event => setPassword2(event.target.value)}
	                required
	            />
	        </Form.Group>
	        </Row>
	        

	       
	        { isActive ?
	        	<Button variant="primary" type="submit" id="submitBtn" style={{backgroundColor: "#A16AE8", borderColor: "#D9A1A0"}} className="mb-3">
	        		Register
	        	</Button>
	        	: 
	        	<Button variant="primary" type="submit" id="submitBtn" style={{backgroundColor: "#A16AE8", borderColor: "#D9A1A0"}} className="mb-3 " disabled>
	        		Register
	        	</Button>
	        
	    	}
	        
	    </Form>

	    </Container>
	    </Col>
	    </Row>
	)
}
