import { Fragment} from 'react';
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import logo from '../components/logo.png'
import {Container, Row, Col, Card} from 'react-bootstrap'

const data = {
	destination: "/"
}
export default function Home() {
	return (
		<Fragment>
		<Row>
		<Col className="offset-3">
			
			<img alt="logo" src={logo} width="700" height="400" className="m-auto"/>
		</Col>
		</Row>
		</Fragment>

	)
}