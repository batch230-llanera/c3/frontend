
import {Container, Navbar, Nav, Image} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'
import {useState, Fragment, useContext} from 'react'
import UserContext from '../UserContext'
import logo from './logo.png'
export default function AppNavbar(){
	// const [user, setUser] = useState(localStorage.getItem('email'));
	//const { user } = useContext(UserContext)
	const [userId, setUserId] = useState(localStorage.getItem("userId"));
	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));
	
	console.log("result of userId from localStorage: ")
	console.log(userId);
	console.log("result of userRole localStorage: ")
	console.log(userRole);
	
	return(
		<Container className="d-inline-block mt-5">
		<Navbar expand="md" className="offset-1">        	

				        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
				        <Navbar.Collapse id="basic-navbar-nav">
				            <Nav className="m-auto">
					            <Nav.Link as={NavLink} to="/"><img
				        	             alt="logo"
				        	             src={logo}
				        	             width="120"
				        	             height="60"
				        	             className="d-flex"
				        	           /></Nav.Link>
				        	    <Nav.Link className="pt-4" as={NavLink} to="/story">Our Story</Nav.Link>
					            <Nav.Link  className="pt-4" as={NavLink} to="/gallery">Gallery</Nav.Link>
					            <Nav.Link className="pt-4" as={NavLink} to="/products">Products</Nav.Link>
					            {(userId !== null)?
					            	<Fragment>
						            	{(userRole)
						            	?
						            	<>
						            		<Nav.Link className="pt-4" as={NavLink} to="/admin">Admin Dashboard</Nav.Link>
						            		<Nav.Link className="pt-4" as={NavLink} to="/photos">Gallery Dashboard</Nav.Link>
						            		<Nav.Link className="pt-4" as={NavLink} to="/logout">Logout</Nav.Link>
						            	</>
						            	:
						            	<Fragment>
						            	    <Nav.Link className="pt-4" as={NavLink} to="/orders">Cart</Nav.Link>
						            		<Nav.Link className="pt-4" as={NavLink} to="/logout">Logout</Nav.Link>
						            	    </Fragment>
						            	            	
						            	}
					            	</Fragment>
					            	:
					            	<Fragment>
						            	<Nav.Link className="pt-4" as={NavLink} to="/login">Login</Nav.Link>
						            	<Nav.Link className="pt-4" as={NavLink} to="/register">Register</Nav.Link>
					            	</Fragment>
					        	}

				            </Nav>
				        </Navbar.Collapse>
					</Navbar>
				</Container>
	)
}






